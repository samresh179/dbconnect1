package com.dbconnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DBCheckController {
	
	@GetMapping("/")
	public String checkConnect() {
		try {
		Class.forName("com.mysql.cj.jdbc.Driver");  
		Connection con=DriverManager.getConnection("jdbc:mysql://10.0.0.3:3306/gcp","root","Mysql@123");   
		con.close();  
		return "success.html";
		}catch(Exception e){ 
			System.out.println(e);
			return "failed.html";
		}  
	}

}
